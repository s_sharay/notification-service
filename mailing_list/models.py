from django.db import models
from phone_field import PhoneField
import pytz
from django.contrib.postgres.fields import ArrayField

import notification_service.settings

# Create your models here.


# Create your models here.

TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))


class Filter(models.TextChoices):
    PHONE = 'P', 'Phone'
    TAG = 'T', 'Tag'


class Status(models.TextChoices):
    SENT = 'S', 'Sent'
    UNSET = 'U', 'Unsent'


class MailingList(models.Model):
    started_at = models.DateTimeField(verbose_name='Begining of the mailing list', auto_now_add=True)
    message = models.TextField(verbose_name='Message')
    filter_property = models.CharField(verbose_name='Property', max_length=1, choices=Filter.choices)
    finished_at = models.DateTimeField(verbose_name='Ending of mailing list', auto_now_add=True)


class Client(models.Model):
    phone = PhoneField(verbose_name='Phone number', blank=True, null=True)
    code = models.CharField(verbose_name='Code of mobile operator', max_length=3, blank=True, null=True)
    tags = ArrayField(models.CharField(max_length=50), blank=True, null=True)
    timezone = models.CharField(choices=TIMEZONES, max_length=32, default=notification_service.settings.TIME_ZONE)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.phone:
            self.code = self.phone[2:5]
        else:
            self.code = None
        super().save(self, force_update=False, using=None, update_fields=None)


class Message(models.Model):
    started_at = models.DateTimeField(verbose_name='Date and time of sending', auto_now_add=True)
    status = models.CharField(verbose_name='Sending status', choices=Status.choices, max_length=1)
    mailing_list_id = models.OneToOneField(MailingList, on_delete=models.CASCADE, related_name='mailing_list_id')
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
